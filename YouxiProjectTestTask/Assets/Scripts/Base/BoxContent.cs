﻿using System;
using System.Collections;
using Base.Interfaces;
using DG.Tweening;
using UnityEngine;
using Zenject;

namespace Base
{
    [Serializable]
    public sealed class BoxContent : IFieldGridCellContent, IMoveable
    {
        public IFieldGridCell GridCell
        {
            get { return _gridCell; }
            set
            {
                _gridCell = value;
                if (value != null)
                    _worldPosition = value.WorldPosition;
            }
        }
    
        public Vector3 WorldPosition
        {
            get { return _worldPosition; }
        }
    
        public float MovingTime
        {
            get { return _movingTime; }
        }
    
        private IFieldGridCell _gridCell;
    
        private Vector3 _worldPosition;

        private readonly float _movingTime;
    
        public event Action<Vector3> Moving; 
    
        public BoxContent(IFieldGridCell cell, float movingTime = 0.5f)
        {
            GridCell = cell;
            _movingTime = movingTime;
        }
    
        public IEnumerator Move(Vector3 to)
        {
            DOTween.To(() => _worldPosition, value =>
            {
                _worldPosition = value;
                Moving?.Invoke(value);

            }, to, _movingTime);
        
            yield return new WaitForSeconds(_movingTime);
        }
    }
}