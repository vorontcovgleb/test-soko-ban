﻿using System;
using Base.Interfaces;
using UnityEngine;

namespace Base
{
    [Serializable]
    public sealed class EndPointContent : IFieldGridCellContent
    {
        public IFieldGridCell GridCell
        {
            get { return _gridCell; }
            set { _gridCell = value; }
        }
    
        public Vector3 WorldPosition
        {
            get { return _gridCell.WorldPosition; }
        }

        private IFieldGridCell _gridCell;
    
        public EndPointContent(IFieldGridCell cell)
        {
            _gridCell = cell;
        }
    }
}