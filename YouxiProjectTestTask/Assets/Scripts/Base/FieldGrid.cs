﻿using System;
using Base.Interfaces;
using Settings;
using UnityEngine;
using Zenject;

namespace Base
{
    [Serializable]
    public sealed class FieldGrid : IFieldGrid
    {
        public int Width
        {
            get { return _settings.Width; }
        }

        public int Height
        {
            get { return _settings.Height; }
        }

        public Vector2 CellSize
        {
            get { return _settings.CellSize; }
        }

        public int CellCount
        {
            get { return _gridCells.Length; }
        }

        public IFieldGridCell this[int index]
        {
            get
            {
                if (index >= 0 && index < _gridCells.Length)
                    return _gridCells[index];

                return null;
            }
        }
    
        public IFieldGridCell this[int row, int column]
        {
            get
            {
                if (row >= 0 && row < Width && column >= 0 && column < Height)
                {
                    return _gridCells[row * Height + column];
                }

                return null;
            }
        }
    
        private LevelFieldSettings _settings;
        private IFieldGridCell[] _gridCells;

        [Inject]
        private void Constructor(LevelFieldSettings settings)
        {
            _settings = settings;
        
            GenerateCells();
        }

        private void GenerateCells()
        {
            var cellCount = Width * Height;
            _gridCells = new IFieldGridCell[cellCount];

            for (var w = 0; w < Width; w++)
            {
                for (var h = 0; h < Height; h++)
                {
                    var position = new Vector2Int(w, h);
                    var localPosition = _settings.CellSize * position;
                    var worldPosition = new Vector3(localPosition.x, 0.0f, localPosition.y);
                    _gridCells[w*Height + h] = new FieldGridCell(position, worldPosition, _settings.CellSize, null);
                }
            }
        }
    
        [Serializable]
        public sealed class FieldGridStartSetter
        {
            private readonly FieldGrid _fieldGrid;

            public FieldGridStartSetter(FieldGrid fieldGrid)
            {
                _fieldGrid = fieldGrid;
            }

            public void SetStartContent()
            {
                for (var w = 0; w < _fieldGrid.Width; w++)
                {
                    for (var h = 0; h < _fieldGrid.Height; h++)
                    {
                        var cell = _fieldGrid[w, h];
                        var cellContentType = GetCellContentTypeByPlan(w, h, _fieldGrid._settings.LevelPlanTexture,
                            _fieldGrid._settings.WallColor, _fieldGrid._settings.PlayerColor, _fieldGrid._settings.EndPointColor, _fieldGrid._settings.BoxColor);
                        var cellContent = GetGridCellContentByType(cellContentType, cell);
                    
                        SetContentByPosition(w, h, cellContent);
                    }
                }
            }

            private GridCellContentType GetCellContentTypeByPlan(int row, int column, Texture2D plan,
                Color wallColor, Color playerColor, Color endPointColor, Color boxColor)
            {
                var cellColor = plan.GetPixel(row, column);
                var cellContentType = GridCellContentType.None;

                if (cellColor == wallColor)
                {
                    cellContentType = GridCellContentType.Wall;
                }
                else if (cellColor == playerColor)
                {
                    cellContentType = GridCellContentType.Player;
                }
                else if (cellColor == endPointColor)
                {
                    cellContentType = GridCellContentType.EndPoint;
                }
                else if (cellColor == boxColor)
                {
                    cellContentType = GridCellContentType.Box;
                }

                return cellContentType;
            }
        
            private IFieldGridCellContent GetGridCellContentByType(GridCellContentType contentType, IFieldGridCell cell)
            {
                switch (contentType)
                {
                    case GridCellContentType.Wall:
                        return new WallContent(cell);
                    case GridCellContentType.Box:
                        return new BoxContent(cell);
                    case GridCellContentType.EndPoint:
                        return new EndPointContent(cell);
                    case GridCellContentType.Player:
                        return new PlayerContent(cell);
                    default:
                        return null;
                }
            }
        
            private void SetContentByPosition(int row, int column, IFieldGridCellContent content)
            {
                var cell = _fieldGrid[row, column];

                if (cell != null)
                {
                    cell.Content = content;
                }
            }
        
            private enum GridCellContentType
            {
                None, // white color (empty cell)
                Wall, // black color
                Player, // red color (player first position)
                EndPoint, // greedColor
                Box // yellow color (box first position)
            }
        }
    }
}