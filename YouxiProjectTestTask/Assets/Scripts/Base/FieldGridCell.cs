﻿using System;
using Base.Interfaces;
using UnityEngine;

namespace Base
{
    [Serializable]
    public sealed class FieldGridCell : IFieldGridCell
    {
        public Vector2Int Position
        {
            get { return _position; }
        }

        public Vector3 WorldPosition
        {
            get { return _worldPosition; }
        }

        public Vector2 Size
        {
            get { return _size; }
        }

        public IFieldGridCellContent Content
        {
            get { return _content; }
            set
            {
                _content = value;
                if (value != null)
                    _content.GridCell = this;
            }
        }

        private readonly Vector2Int _position;
        private readonly Vector3 _worldPosition;
    
        private readonly Vector2 _size;

        private IFieldGridCellContent _content;

        public FieldGridCell(Vector2Int position, Vector3 worldPosition, Vector2 size, IFieldGridCellContent content)
        {
            _position = position;
            _worldPosition = worldPosition;
            _content = content;
            _size = size;
        }
    }
}
