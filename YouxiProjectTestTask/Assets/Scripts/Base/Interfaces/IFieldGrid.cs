﻿namespace Base.Interfaces
{
    public interface IFieldGrid
    {
        int Width { get; }
        int Height { get; }
        int CellCount { get; }
    }
}