﻿using UnityEngine;

namespace Base.Interfaces
{
    public interface IFieldGridCell
    {
        Vector2Int Position { get; }
        Vector3 WorldPosition { get; }
    
        Vector2 Size { get; }
    
        IFieldGridCellContent Content { get; set; }
    }
}