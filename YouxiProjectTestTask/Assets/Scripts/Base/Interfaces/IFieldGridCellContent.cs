﻿using UnityEngine;

namespace Base.Interfaces
{
    public interface IFieldGridCellContent
    {
        Vector3 WorldPosition { get; }
    
        IFieldGridCell GridCell { get; set; }
    }
}