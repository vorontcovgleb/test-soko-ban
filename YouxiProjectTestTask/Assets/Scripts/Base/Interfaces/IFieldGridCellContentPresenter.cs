﻿using UnityEngine;

namespace Base.Interfaces
{
    public interface IFieldGridCellContentPresenter<T, K> where T : MonoBehaviour where K : IFieldGridCellContent
    {
        T Behaviour { get; }
        K Content { get; }
    }
}