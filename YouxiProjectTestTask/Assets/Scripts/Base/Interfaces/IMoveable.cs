﻿using System;
using System.Collections;
using UnityEngine;

namespace Base.Interfaces
{
    public interface IMoveable
    {
        float MovingTime { get; }
    
        event Action<Vector3> Moving;
    
        IEnumerator Move(Vector3 to);
    }
}