﻿using System;
using System.Collections;
using Base.Interfaces;
using DG.Tweening;
using UnityEngine;
using Zenject;

namespace Base
{
    [Serializable]
    public sealed class PlayerContent : IFieldGridCellContent, IMoveable
    {
        public IFieldGridCell GridCell
        {
            get { return _gridCell; }
            set
            {
                _gridCell = value;
                if (value != null)
                    _currentWorldPosition = value.WorldPosition;
            }
        }
    
        public Vector3 WorldPosition
        {
            get { return _currentWorldPosition; }
        }
    
        public float MovingTime
        {
            get { return _movingTime; }
        }
    
        private IFieldGridCell _gridCell;
    
        private Vector3 _currentWorldPosition;

        private readonly float _movingTime;
    
        public event Action<Vector3> Moving; 
    
        public PlayerContent(IFieldGridCell cell, float movingTime = 0.5f)
        {
            GridCell = cell;
            _movingTime = movingTime;
        }
    
        public IEnumerator Move(Vector3 to)
        {
            DOTween.To(() => _currentWorldPosition, value =>
            {
                _currentWorldPosition = value;
                Moving?.Invoke(value);

            }, to, _movingTime);
        
            yield return new WaitForSeconds(_movingTime);
        }
    }
}