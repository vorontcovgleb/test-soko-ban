﻿using Presenters;
using UnityEngine;
using Zenject;

[DisallowMultipleComponent]
public sealed class CameraController : MonoBehaviour
{
    private FieldGridPresenter _fieldGridPresenter;

    [Inject]
    private void Constructor(FieldGridPresenter fieldGridPresenter)
    {
        _fieldGridPresenter = fieldGridPresenter;
    }

    private void Start()
    {
        var fieldGridSize = new Vector3(_fieldGridPresenter.FieldGrid.Width * _fieldGridPresenter.FieldGrid.CellSize.x, 0.0f,
            _fieldGridPresenter.FieldGrid.Height * _fieldGridPresenter.FieldGrid.CellSize.y);

        var fieldGridCenter = _fieldGridPresenter.transform.position + fieldGridSize * 0.5f - new Vector3(0.5f, 0.0f, 0.5f);
        var newPosition = new Vector3(0.0f, fieldGridSize.x > fieldGridSize.y ? fieldGridSize.x : fieldGridSize.y, 0.0f) + fieldGridCenter;

        this.transform.position = newPosition;
    }
}
