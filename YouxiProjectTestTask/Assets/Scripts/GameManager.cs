﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

[DisallowMultipleComponent]
public sealed class GameManager : MonoBehaviour
{
    public void ReloadCurrentLevel()
    {
        var currentIndex = SceneManager.GetActiveScene().buildIndex;
        
        UnloadCurrentScene(() =>
        {
            LoadSceneAdditiveByIndex(currentIndex);
        });
    }
    
    public void LoadNextLevel()
    {
        var currentIndex = SceneManager.GetActiveScene().buildIndex;
        
        UnloadCurrentScene(() =>
        {
            currentIndex++;
            if (currentIndex < SceneManager.sceneCountInBuildSettings)
            {
                LoadSceneAdditiveByIndex(currentIndex);
            }
        });
    }
    
    public void UnloadCurrentScene(Action callback = null)
    {
        var index = SceneManager.GetActiveScene().buildIndex;
        var operation = SceneManager.UnloadSceneAsync(index);
        operation.completed += asyncOperation => { callback?.Invoke();};
    }
    
    public void UnloadSceneByIndex(int index)
    {
        SceneManager.UnloadSceneAsync(index);
    }
    
    public void LoadSceneAdditiveByIndex(int index)
    {
        var operation = SceneManager.LoadSceneAsync(index, LoadSceneMode.Additive);
        operation.completed += asyncOperation =>
        {
            SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(index));
        };
    }
}
