using UI;
using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;

[DisallowMultipleComponent]
public sealed class GameObserver : MonoBehaviour
{
    private PlayerController _playerController;
    private UIManager _uiManager;

    [Inject]
    private void Constructor(PlayerController playerController, UIManager uiManager)
    {
        _playerController = playerController;
        _uiManager = uiManager;
    }
    
    private void OnEnable()
    {
        _playerController.PlayerReachedEndPoint += PlayerControllerOnPlayerReachedEndPoint;
    }

    private void OnDisable()
    {
        _playerController.PlayerReachedEndPoint -= PlayerControllerOnPlayerReachedEndPoint;
    }
    
    private void PlayerControllerOnPlayerReachedEndPoint()
    {
        EndLevel();
    }

    private void EndLevel()
    {
        _uiManager.ShowVictoryMenu();
    }
}