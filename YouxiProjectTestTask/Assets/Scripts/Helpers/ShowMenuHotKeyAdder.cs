﻿using UI;
using UnityEngine;
using Zenject;

namespace Helpers
{
    public sealed class ShowMenuHotKeyAdder : ITickable
    {
        private GameManager _gameManager;
        private UIManager _uiManager;
        
        [Inject]
        private void Constructor(GameManager gameManager, UIManager uiManager)
        {
            _gameManager = gameManager;
            _uiManager = uiManager;
        }
    
        public void Tick()
        {
            if (Input.GetKeyUp(KeyCode.Escape))
            {
                if (!_uiManager.IsShowedAnyWindows)
                {
                    _gameManager.UnloadCurrentScene();
                    _uiManager.ShowMainMenu();
                }
                    
            }
        }
    }
}