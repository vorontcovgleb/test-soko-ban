using Base;
using Presenters;
using Settings;
using UnityEngine;
using Zenject;

namespace Installers
{
    public class FieldGridInstaller : MonoInstaller
    {
        [SerializeField]
        private LevelFieldSettings _levelFieldSettings;
        [SerializeField]
        private FieldGridPresenter _fieldGridPresenter;
    
        public override void InstallBindings()
        {
            Container.Bind<LevelFieldSettings>().FromInstance(_levelFieldSettings).AsSingle();

            Container.Bind<FieldGrid>().FromNew().AsCached();

            Container.Bind<FieldGridPresenter>().FromInstance(_fieldGridPresenter).AsSingle();
        }
    }
}