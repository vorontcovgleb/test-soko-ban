using Presenters;
using Zenject;

namespace Installers
{
    public class PlayerInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<PlayerContentPresenter>().FromComponentSibling();
            
            Container.Bind<PlayerController>().FromComponentInHierarchy().AsCached();
        }
    }
}