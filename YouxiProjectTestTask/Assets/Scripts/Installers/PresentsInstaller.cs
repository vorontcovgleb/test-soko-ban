﻿using Base;
using Presenters;
using UnityEngine;
using Zenject;

namespace Installers
{
    [DisallowMultipleComponent]
    public sealed class PresentsInstaller : MonoInstaller
    {
        [SerializeField]
        private GameObject _emptyFieldElementPresenter;
    
        [SerializeField, Space]
        private GameObject _wallPresenter;
        [SerializeField]
        private GameObject _endPointPresenter;
        [SerializeField]
        private GameObject _boxPresenter;
        [SerializeField]
        private GameObject _playerPresenter;

        public override void InstallBindings()
        {
            Container.BindFactory<FieldGridCell, FieldGridCellPresenter, FieldGridCellPresenter.Factory>()
                .FromComponentInNewPrefab(_emptyFieldElementPresenter);

            Container.BindFactory<WallContent, WallContentPresenter, WallContentPresenter.Factory>().FromComponentInNewPrefab(_wallPresenter);
            Container.BindFactory<BoxContent, BoxContentPresenter, BoxContentPresenter.Factory>().FromComponentInNewPrefab(_boxPresenter);
            Container.BindFactory<EndPointContent, EndPointContentPresenter, EndPointContentPresenter.Factory>()
                .FromComponentInNewPrefab(_endPointPresenter);
            Container.BindFactory<PlayerContent, PlayerContentPresenter, PlayerContentPresenter.Factory>()
                .FromComponentInNewPrefab(_playerPresenter);
        }
    }
}
