﻿using Helpers;
using UI;
using UnityEngine;
using Zenject;

namespace Installers
{
    [DisallowMultipleComponent]
    public sealed class StartSceneInstaller : MonoInstaller
    {
        [SerializeField]
        private GameObject _uiManager;
        [SerializeField]
        private GameObject _gameManager;
    
        public override void InstallBindings()
        {
            Container.Bind<ITickable>().To<ReloadHotKeyAdder>().AsSingle();
            Container.Bind<ITickable>().To<ShowMenuHotKeyAdder>().AsSingle();

            Container.Bind<UIManager>().FromComponentOn(_uiManager).AsSingle();
            Container.Bind<GameManager>().FromComponentOn(_gameManager).AsSingle();
        }
    }
}