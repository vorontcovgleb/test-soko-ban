﻿using System;
using System.Collections;
using Base;
using Base.Interfaces;
using Presenters;
using UnityEngine;
using Zenject;

[DisallowMultipleComponent]
public sealed class PlayerController : MonoBehaviour
{
    private PlayerContentPresenter _playerPresenter;
    private FieldGrid _fieldGrid;
    
    private bool _isMoving;
    private bool _isGetInput = true;

    public event Action PlayerReachedEndPoint;

    [Inject]
    private void Constructor(FieldGrid fieldGrid, PlayerContentPresenter playerPresenter)
    {
        _fieldGrid = fieldGrid;
        _playerPresenter = playerPresenter;
    }

    private void Update()
    {
        if(_isMoving || !_isGetInput)
            return;
        
        if (Input.GetKeyUp(KeyCode.LeftArrow))
        {
            StartCoroutine(Move(new Vector2Int(-1, 0)));
        }
        else if (Input.GetKeyUp(KeyCode.RightArrow))
        {
            StartCoroutine(Move(new Vector2Int(1, 0)));
        }
        else if (Input.GetKeyUp(KeyCode.UpArrow))
        {
            StartCoroutine(Move(new Vector2Int(0, 1)));
        }
        else if (Input.GetKeyUp(KeyCode.DownArrow))
        {
            StartCoroutine(Move(new Vector2Int(0, -1)));
        }
    }

    private IEnumerator Move(Vector2Int direction)
    {
        _isMoving = true;
        
        var playerCell = _playerPresenter.Content.GridCell;
        var newPosition = playerCell.Position + direction;

        var nextCell = _fieldGrid[newPosition.x, newPosition.y];
        if (nextCell != null)
        {
            if (nextCell.Content == null || nextCell.Content is EndPointContent)
            {
                yield return _playerPresenter.Content.Move(nextCell.WorldPosition);

                if (nextCell.Content is EndPointContent)
                {
                    _isGetInput = false;

                    PlayerReachedEndPoint?.Invoke();
                }
                
                playerCell.Content = null;
                nextCell.Content = _playerPresenter.Content;
            }
            else if (nextCell.Content is WallContent)
            {
                yield return null;
            }
            else if (nextCell.Content is BoxContent)
            {
                var postNextCellPosition = nextCell.Position + direction;
                var postNextCell = _fieldGrid[postNextCellPosition.x, postNextCellPosition.y];

                if (postNextCell != null)
                {
                    if (postNextCell.Content == null)
                    {
                        StartCoroutine(((IMoveable) nextCell.Content).Move(postNextCell.WorldPosition));
                        yield return _playerPresenter.Content.Move(nextCell.WorldPosition);

                        postNextCell.Content = nextCell.Content;
                        nextCell.Content = _playerPresenter.Content;
                        playerCell.Content = null;
                    }
                    else
                    {
                        yield return null;
                    }
                }

                yield return null;
            }
        }
        
        _isMoving = false;
    }
}
