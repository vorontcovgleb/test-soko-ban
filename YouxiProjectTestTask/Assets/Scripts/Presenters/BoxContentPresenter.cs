﻿using Base;
using Base.Interfaces;
using UnityEngine;
using Zenject;

namespace Presenters
{
    [DisallowMultipleComponent]
    public sealed class BoxContentPresenter : MonoBehaviour, IFieldGridCellContentPresenter<BoxContentPresenter, BoxContent>
    {
        public BoxContentPresenter Behaviour
        {
            get { return this; }
        }

        public BoxContent Content
        {
            get { return _content; }
        }
    
        private BoxContent _content;
    
        [Inject]
        private void Constructor(BoxContent content)
        {
            _content = content;
        }
    
        private void Start()
        {
            this.transform.position = Content.WorldPosition;
        }
    
        private void OnEnable()
        {
            Content.Moving += OnMoving;
        }

        private void OnDisable()
        {
            Content.Moving -= OnMoving;
        }

        private void OnMoving(Vector3 newPosition)
        {
            this.transform.position = newPosition;
        }
    
        public sealed class Factory : PlaceholderFactory<BoxContent, BoxContentPresenter>
        {
        }
    }
}