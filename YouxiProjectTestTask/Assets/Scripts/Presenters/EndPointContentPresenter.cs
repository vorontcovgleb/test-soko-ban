﻿using Base;
using Base.Interfaces;
using UnityEngine;
using Zenject;

namespace Presenters
{
    [DisallowMultipleComponent]
    public sealed class EndPointContentPresenter : MonoBehaviour, IFieldGridCellContentPresenter<EndPointContentPresenter, EndPointContent>
    {
        public EndPointContentPresenter Behaviour
        {
            get { return this; }
        }

        public EndPointContent Content
        {
            get { return _content; }
        }
    
        private EndPointContent _content;
    
        [Inject]
        private void Constructor(EndPointContent content)
        {
            _content = content;
        }
    
        private void Start()
        {
            this.transform.position = Content.WorldPosition + new Vector3(0.0f, -0.499f, 0.0f);
        }
    
        public sealed class Factory : PlaceholderFactory<EndPointContent, EndPointContentPresenter>
        {
        }
    }
}