﻿using Base;
using UnityEngine;
using Zenject;

namespace Presenters
{
    [DisallowMultipleComponent]
    public sealed class FieldGridCellPresenter : MonoBehaviour
    {
        public FieldGridCell GridCell
        {
            get { return _gridCell; }
        }
        
        private FieldGridCell _gridCell;

        [Inject]
        private void Constructor(FieldGridCell gridCell)
        {
            _gridCell = gridCell;
        }

        private void Start()
        {
            this.transform.position = _gridCell.WorldPosition + new Vector3(0.0f, -0.5f, 0.0f);
        }

        public sealed class Factory : PlaceholderFactory<FieldGridCell, FieldGridCellPresenter>
        {
        }
    }
}