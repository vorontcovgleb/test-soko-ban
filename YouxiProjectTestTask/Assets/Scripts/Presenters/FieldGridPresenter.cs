﻿using Base;
using UnityEngine;
using Zenject;

namespace Presenters
{
    [DisallowMultipleComponent]
    public sealed class FieldGridPresenter : MonoBehaviour
    {
        public FieldGrid FieldGrid
        {
            get { return _fieldGrid; }
        }
        
        private FieldGrid _fieldGrid;
        
        private FieldGridCellPresenter.Factory _cellsFactory;

        private BoxContentPresenter.Factory _boxFactory;
        private WallContentPresenter.Factory _wallFactory;
        private EndPointContentPresenter.Factory _endPointFactory;
        private PlayerContentPresenter.Factory _playerFactory;

        [Inject]
        private void Constructor(FieldGrid fieldGrid, FieldGridCellPresenter.Factory cellsFactory, 
            BoxContentPresenter.Factory boxFactory, WallContentPresenter.Factory wallFactory,
            EndPointContentPresenter.Factory endPointFactory, PlayerContentPresenter.Factory playerFactory)
        {
            _fieldGrid = fieldGrid;
            _cellsFactory = cellsFactory;
            
            _boxFactory = boxFactory;
            _wallFactory = wallFactory;
            _endPointFactory = endPointFactory;
            _playerFactory = playerFactory;
            
            CreateCellPresenters();
            SetStartPresenters();
        }

        private void CreateCellPresenters()
        {
            for (var i = 0; i < _fieldGrid.CellCount; i++)
            {
                var cellPresenter = _cellsFactory.Create(_fieldGrid[i] as FieldGridCell);
                cellPresenter.transform.parent = this.transform;
            }
        }
        
        private void SetStartPresenters()
        {
            var startFieldGridSetter = new FieldGrid.FieldGridStartSetter(_fieldGrid);
            startFieldGridSetter.SetStartContent();
            
            for (var w = 0; w < _fieldGrid.Width; w++)
            {
                for (var h = 0; h < _fieldGrid.Height; h++)
                {
                    var cell = _fieldGrid[w, h];

                    if (cell?.Content != null)
                    {
                        if (cell.Content is WallContent)
                        {
                            var wallContentPresenter = _wallFactory.Create((WallContent)cell.Content);
                            wallContentPresenter.Behaviour.transform.parent = this.transform;
                        }
                        else if (cell.Content is BoxContent)
                        {
                            var boxContentPresenter = _boxFactory.Create((BoxContent) cell.Content);
                            boxContentPresenter.Behaviour.transform.parent = this.transform;
                        }
                        else if (cell.Content is PlayerContent)
                        {
                            var playerContentPresenter = _playerFactory.Create((PlayerContent) cell.Content);
                            playerContentPresenter.Behaviour.transform.parent = this.transform;
                        }
                        else if (cell.Content is EndPointContent)
                        {
                            var endPointContentPresenter = _endPointFactory.Create((EndPointContent) cell.Content);
                            endPointContentPresenter.Behaviour.transform.parent = this.transform;
                        }
                    }
                }
            }
        }
    }
}
