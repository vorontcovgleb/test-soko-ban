﻿using Base;
using Base.Interfaces;
using UnityEngine;
using Zenject;

namespace Presenters
{
    [DisallowMultipleComponent]
    public sealed class PlayerContentPresenter : MonoBehaviour, IFieldGridCellContentPresenter<PlayerContentPresenter, PlayerContent>
    {
        public PlayerContentPresenter Behaviour
        {
            get { return this; }
        }

        public PlayerContent Content
        {
            get { return _content; }
        }
    
        private PlayerContent _content;
    
        [Inject]
        private void Constructor(PlayerContent content)
        {
            _content = content;
        }
    
        private void Start()
        {
            this.transform.position = Content.WorldPosition;
        }

        private void OnEnable()
        {
            Content.Moving += OnMoving;
        }

        private void OnDisable()
        {
            Content.Moving -= OnMoving;
        }

        private void OnMoving(Vector3 newPosition)
        {
            this.transform.position = newPosition;
        }

        public sealed class Factory : PlaceholderFactory<PlayerContent, PlayerContentPresenter>
        {
        }
    }
}