﻿using Base;
using Base.Interfaces;
using UnityEngine;
using Zenject;

namespace Presenters
{
    [DisallowMultipleComponent]
    public sealed class WallContentPresenter : MonoBehaviour, IFieldGridCellContentPresenter<WallContentPresenter, WallContent>
    {
        public WallContentPresenter Behaviour
        {
            get { return this; }
        }

        public WallContent Content
        {
            get { return _content; }
        }
    
        private WallContent _content;
    
        [Inject]
        private void Constructor(WallContent content)
        {
            _content = content;
        }
    
        private void Start()
        {
            this.transform.position = Content.WorldPosition;
        }

        public sealed class Factory : PlaceholderFactory<WallContent, WallContentPresenter>
        {
        }
    }
}