﻿using UnityEngine;

namespace Settings
{
    [CreateAssetMenu(menuName = "Settings/LevelFieldSettings", fileName = "LevelFieldSettings")]
    public sealed class LevelFieldSettings : ScriptableObject
    {
        [SerializeField]
        private Color _wallColor = Color.black;
        [SerializeField]
        private Color _boxColor = new Color(1.0f, 1.0f, 0.0f);
        [SerializeField]
        private Color _playerColor = Color.red;
        [SerializeField]
        private Color _endPointColor = Color.green;
        
        [SerializeField]
        private Texture2D _levelPlanTexture;

        [SerializeField]
        private Vector2 _cellSize = Vector2.one;
    
        public int Width
        {
            get { return _levelPlanTexture.width; }
        }

        public int Height
        {
            get { return _levelPlanTexture.height; }
        }

        public Texture2D LevelPlanTexture
        {
            get { return _levelPlanTexture; }
        }

        public Vector2 CellSize
        {
            get { return _cellSize; }
        }

        public Color WallColor
        {
            get { return _wallColor; }
        }

        public Color BoxColor
        {
            get { return _boxColor; }
        }

        public Color PlayerColor
        {
            get { return _playerColor; }
        }

        public Color EndPointColor
        {
            get { return _endPointColor; }
        }
    }
}