﻿using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace UI
{
    [DisallowMultipleComponent]
    public sealed class MainMenuUIController : UIWindow
    {
        [SerializeField]
        private Button _startButton;

        private GameManager _gameManager;

        [Inject]
        private void Constructor(GameManager gameManager)
        {
            _gameManager = gameManager;
        }

        private void OnEnable()
        {
            if (_startButton != null)
            {
                _startButton.onClick.AddListener(OnStartButtonClick);
            }
        }

        private void OnDisable()
        {
            if (_startButton != null)
            {
                _startButton.onClick.RemoveListener(OnStartButtonClick);
            }
        }

        private void OnStartButtonClick()
        {
            this.Hide();
            
            _gameManager.LoadSceneAdditiveByIndex(1);
        }
    }
}
