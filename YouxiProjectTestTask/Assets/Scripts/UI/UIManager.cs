﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace UI
{
    [DisallowMultipleComponent]
    public sealed class UIManager : MonoBehaviour
    {
        [SerializeField]
        private UIWindow _mainMenu;
        [SerializeField]
        private UIWindow _victoryMenu;
        [SerializeField]
        private UIWindow _victoryEndMenu;

        public bool IsShowedAnyWindows
        {
            get { return _mainMenu.IsShowed || _victoryMenu.IsShowed || _victoryEndMenu.IsShowed; }
        }

        private void Start()
        {
            _mainMenu.Show();
        }

        public void ShowVictoryMenu()
        {
            var currentIndex = SceneManager.GetActiveScene().buildIndex + 1;

            if (currentIndex < SceneManager.sceneCountInBuildSettings)
            {
                _victoryMenu.Show();
            }
            else
            {
                _victoryEndMenu.Show();
            }
        }

        public void ShowMainMenu()
        {
            _mainMenu.Show();
        }
    }
}
