﻿using UnityEngine;

namespace UI
{
    public class UIWindow : MonoBehaviour
    {
        public virtual bool IsShowed
        {
            get { return _isShowed; }
            protected set { _isShowed = value; }
        }

        private bool _isShowed;
        
        public virtual void Show()
        {
            gameObject.SetActive(true);
            _isShowed = true;
        }

        public virtual void Hide()
        {
            gameObject.SetActive(false);
            _isShowed = false;
        }
    }
}
