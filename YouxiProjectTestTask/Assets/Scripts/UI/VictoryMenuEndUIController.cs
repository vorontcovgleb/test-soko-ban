﻿using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace UI
{
    [DisallowMultipleComponent]
    public class VictoryMenuEndUIController : UIWindow
    {
        [SerializeField]
        private Button _toMainMenuButton;

        [SerializeField]
        private MainMenuUIController _mainMenuUiController;
        
        private GameManager _gameManager;

        [Inject]
        private void Constructor(GameManager gameManager)
        {
            _gameManager = gameManager;
        }
        
        private void OnEnable()
        {
            if (_toMainMenuButton != null)
            {
                _toMainMenuButton.onClick.AddListener(OnToMainMenuButtonClick);
            }
        }
        
        private void OnDisable()
        {
            if (_toMainMenuButton != null)
            {
                _toMainMenuButton.onClick.RemoveListener(OnToMainMenuButtonClick);
            }
        }

        private void OnToMainMenuButtonClick()
        {
            this.Hide();
            
            _gameManager.UnloadCurrentScene();
            
            if (_mainMenuUiController != null)
                _mainMenuUiController.Show();
        }
    }
}
