﻿using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace UI
{
    [DisallowMultipleComponent]
    public sealed class VictoryMenuUIController : UIWindow
    {
        [SerializeField]
        private Button _nextLevelButton;
        [SerializeField]
        private Button _toMainMenuButton;

        [SerializeField]
        private MainMenuUIController _mainMenuUiController;
        
        private GameManager _gameManager;

        [Inject]
        private void Constructor(GameManager gameManager)
        {
            _gameManager = gameManager;
        }

        private void OnEnable()
        {
            if (_nextLevelButton != null)
            {
                _nextLevelButton.onClick.AddListener(OnNextLevelButtonClick);
            }

            if (_toMainMenuButton != null)
            {
                _toMainMenuButton.onClick.AddListener(OnToMainMenuButtonClick);
            }
        }
        
        private void OnDisable()
        {
            if (_nextLevelButton != null)
            {
                _nextLevelButton.onClick.RemoveListener(OnNextLevelButtonClick);
            }

            if (_toMainMenuButton != null)
            {
                _toMainMenuButton.onClick.RemoveListener(OnToMainMenuButtonClick);
            }
        }

        private void OnToMainMenuButtonClick()
        {
            this.Hide();

            _gameManager.UnloadCurrentScene();
            
            if (_mainMenuUiController != null)
                _mainMenuUiController.Show();
        }

        private void OnNextLevelButtonClick()
        {
            this.Hide();
            
            _gameManager.LoadNextLevel();
        }
    }
}
